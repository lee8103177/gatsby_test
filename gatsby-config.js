module.exports = {
  siteMetadata: {
    siteUrl: "https://www.yourdomain.tld",
    title: "gatsby_test",
  },
  plugins: ["gatsby-plugin-sass", "gatsby-plugin-gatsby-cloud"],
};
